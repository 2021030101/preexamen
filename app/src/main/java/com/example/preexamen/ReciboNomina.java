package com.example.preexamen;

import java.util.Random;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;

    // Constructores

    public ReciboNomina() {
        this.numRecibo = this.generarNumero();
        this.nombre = "";
        this.horasTrabNormal = 0;
        this.horasTrabExtras = 0;
        this.puesto = 0;
    }

    public ReciboNomina(String nombre, float horasTrabNormal, float horasTrabExtras, int puesto) {
        this.numRecibo = this.generarNumero();
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
    }

    // Get y Set
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    // Métodos
    public int generarNumero() {
        Random r = new Random();
        return Math.abs(r.nextInt() % 1000);
    }

    public float calcularSubtotal() {
        int pagoHora = 200;
        float subtotal = 0.0f;
        switch (this.puesto) {
            case 1:
                pagoHora += (pagoHora * 0.2f);
                break;
            case 2:
                pagoHora += (pagoHora * 0.5f);
                break;
            case 3:
                pagoHora *= 2;
                break;
        }
        subtotal = (pagoHora * this.horasTrabNormal);
        subtotal += (this.horasTrabExtras * 2) * pagoHora;
        return subtotal;
    }

    public float calcularImpuesto() {
        return this.calcularSubtotal() * 0.16f;
    }

    public float calcularTotal() {
        return this.calcularSubtotal() - this.calcularImpuesto();
    }
}
