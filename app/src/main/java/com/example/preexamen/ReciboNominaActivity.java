package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {

    private ReciboNomina reciboNomina;
    private TextView lblNombre, lblNumeroRecibo, lblSubtotal, lblImpuestos, lblTotal;
    private EditText txtHorasNormal, txtHorasExtras;
    private RadioGroup radioGroup;
    private RadioButton rdbPuesto1, rdbPuesto2, rdbPuesto3;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    private boolean isVacio() {
        return this.txtHorasNormal.getText().toString().equals("") ||
                this.txtHorasExtras.getText().toString().equals("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        this.init();

        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "Hay campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                reciboNomina.setHorasTrabNormal(Integer.parseInt(txtHorasNormal.getText().toString()));
                reciboNomina.setHorasTrabExtras(Integer.parseInt(txtHorasExtras.getText().toString()));
                if(rdbPuesto1.isChecked()) { reciboNomina.setPuesto(1); }
                if(rdbPuesto2.isChecked()) { reciboNomina.setPuesto(2); }
                if(rdbPuesto3.isChecked()) { reciboNomina.setPuesto(3); }
                lblSubtotal.setText(String.valueOf(reciboNomina.calcularSubtotal()));
                lblImpuestos.setText(String.valueOf(reciboNomina.calcularImpuesto()));
                lblTotal.setText(String.valueOf(reciboNomina.calcularTotal()));
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                txtHorasNormal.setText("");
                txtHorasExtras.setText("");
                lblSubtotal.setText("");
                lblImpuestos.setText("");
                lblTotal.setText("");
            }
        });

        this.btnCerrar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.lblNombre = (TextView) findViewById(R.id.lblNombre);
        this.lblNumeroRecibo = (TextView) findViewById(R.id.lblNumeroRecibo);
        this.txtHorasNormal = (EditText) findViewById(R.id.txtHorasTrabajadas);
        this.txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtras);
        this.radioGroup = (RadioGroup) findViewById(R.id.rdbGroup);
        this.rdbPuesto1 = (RadioButton) findViewById(R.id.rdbPuesto1);
        this.rdbPuesto2 = (RadioButton) findViewById(R.id.rdbPuesto2);
        this.rdbPuesto3 = (RadioButton) findViewById(R.id.rdbPuesto3);
        this.lblSubtotal = (TextView) findViewById(R.id.txtSubtotal);
        this.lblImpuestos = (TextView) findViewById(R.id.txtImpuesto);
        this.lblTotal = (TextView) findViewById(R.id.txtTotal);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
        this.rdbPuesto1.setChecked(true);
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblNombre.setText(nombre);
        this.reciboNomina = new ReciboNomina();
        this.lblNumeroRecibo.setText(String.valueOf(reciboNomina.getNumRecibo()));
    }
}